package ejemplos.internas;

/**
 * Clase Alumno, en el contexto de un sistema bibliotecario
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class Alumno {
    
	private String nombre;
    private String direccion;
    private String carrera;  
    private PrestamoLibro[] librosPrestados;
    private int idx = 0;
        
    public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCarrera() {
		return carrera;
	}

	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}

	public PrestamoLibro[] getLibrosPrestados() {
		return librosPrestados;
	}

	public void setLibrosPrestados(PrestamoLibro[] librosPrestados) {
		this.librosPrestados = librosPrestados;
	}
		
    public Alumno(String n, String d, String c) {
        nombre = n;
        direccion = d;
        carrera = c;
        librosPrestados = new PrestamoLibro[10];
    }
    
    public void prestarNuevoLibro(Libro l) {
        if (idx < librosPrestados.length) {
            librosPrestados[idx++] = new PrestamoLibro(this, l);
        }
    }
    
    public void imprimirLibrosPrestados() {
        for (int i = 0; i < idx; i++) {
            System.out.printf("\nAlumno %s, Libro %s", this.nombre, librosPrestados[i].getLibro().getTitulo());
        }
    }
    
    /************************************/
    /************************************/
    public class Libro {
        private String titulo;
        private String autor;
        private int anhoPublicacion;   
        
        public Libro(String t, String a, int an) {
            titulo = t;
            autor = a;
            anhoPublicacion = an;
        }

        /**
         * @return the titulo
         */
        public String getTitulo() {
            return titulo;
        }

        /**
         * @return the autor
         */
        public String getAutor() {
            return autor;
        }

        /**
         * @return the anhoPublicacion
         */
        public int getAnhoPublicacion() {
            return anhoPublicacion;
        }
    }
    
    /************************************/
    /************************************/
    public class PrestamoLibro {
        private Alumno a;
        private Libro l;
        
        public PrestamoLibro() {
            a = null;
            l = null;
        }
        
        public PrestamoLibro(Alumno a, Libro l) {
            this.a = a;
            this.l = l;
        }

        /**
         * @return the a
         */
        public Alumno getAlumno() {
            return a;
        }

        /**
         * @return the l
         */
        public Libro getLibro() {
            return l;
        }

    }

} //Fin de clase Alumno
