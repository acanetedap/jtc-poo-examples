package ejemplos.abstractas;

/**
 * Clase TestAbstract
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class MainPruebaAbstract {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

    	//El vector es del tipo Profesor
        Profesor[] profs = new Profesor[4];
        
        //Podemos guardar cualquier subtipo de Profesor (clases derivadas)
        profs[0] = new ProfesorAyudante();        
        profs[1] = new ProfesorContratado();
        profs[2] = new ProfesorEscalafonado();
        profs[3] = new ProfesorContratado();
        
        for (int i = 0; i < profs.length; i++) {
            System.out.printf("\narray[%d]: ", i);
            profs[i].imprimir();
        }

    } //Fin de main

} //Fin de clase TestAbstract
