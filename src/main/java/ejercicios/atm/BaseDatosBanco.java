package ejercicios.atm;

import java.util.ArrayList;
import java.util.List;

import ejercicios.atm.exception.ArchivoExcepcion;


public class BaseDatosBanco {
    // TODO Crear una instancia de la clase Logger. 
	// TODO: Crear excepcion AplicacionError dentro del paquete ejercicios.atam.excepcion. 
	//       ver ejemplo ArchivoException
	
	private static final String NOMBRE_ARCHIVO = "cuentas.txt";
    private static List<Cuenta> listaCuentas;
    private ArchivoUtil archivoUtil;
    
    public BaseDatosBanco() {
    	archivoUtil = new ArchivoUtil(NOMBRE_ARCHIVO);
    	precargarDatosDesdeArchivo();
    }

    
    /**
     * Metodo que obtiene los datos desde archivo y lo carga el la varible de instancia listaCuentas.
     */
    private void precargarDatosDesdeArchivo() {        
        List<String> datosArchivo;
		try {
			//TODO: loguear que se carga los datos desde archivo, utilizar nivel INFO
			datosArchivo = archivoUtil.obtenerDatosDesdeArchivo();
			listaCuentas = listaStringAListaCuenta(datosArchivo);
		} catch (ArchivoExcepcion e) {
			// TODO: loguear error con el nivel ERROR
			// TODO: lanzar excepcion AplicacionExcepcion, el mensaje deberia indicar problemas con la base de datos
		}
        
    }
        
    //Operaciones
    private Cuenta buscarCuenta(int nroCuentaABuscar) {
        for (Cuenta cuenta : listaCuentas) {
            if (cuenta.getNumeroCuenta() == nroCuentaABuscar) {
                return cuenta;
            }
        }
        return null;
    }
    
    public boolean autenticarUsuario(int nroCta, int pin) {
        Cuenta cta = buscarCuenta(nroCta); //existe la cuenta?
        if (cta != null) {
            //cuenta existente
            if (cta.validarPIN(pin) == true) {
                //pin valido
                return true;
            } else {
                //cuenta valida, pero pin invalido
                return false;
            }
        } else {
            //Cuenta inexistente en la base de datos
            return false;
        }
    }
    
    public double obtenerSaldoDisponible(int nroCuenta) {
    	double saldoDisponible;
    	Cuenta cuentaEncontrada = buscarCuenta(nroCuenta);
    	if (cuentaEncontrada != null) {
    		saldoDisponible = cuentaEncontrada.obtenerSaldoDisponible();
    	} else {
    		//Manejo del error aqui... mostrar un mensaje 
    		saldoDisponible = -1;
    	}
    	return saldoDisponible;
    }
    

    public void debitar(int nroCuenta, double monto) {
       
    	for (Cuenta cuenta : listaCuentas) {
			if (cuenta.getNumeroCuenta() == nroCuenta){
				cuenta.debitar(monto);
			}
		}
    	actualizarArchivo();
    	
    }
    
    public void acreditar(int nroCuenta, double monto) {
    	
    	for (Cuenta cuenta : listaCuentas) {
			if (cuenta.getNumeroCuenta() == nroCuenta){
				cuenta.acreditar(monto);
			}
		}
    	actualizarArchivo();
    }
    
    
    /**
     * Actualiza el archivo con la lista de cuentas actualizada luego realizar alguna operacion
     */
    private void actualizarArchivo()
    {
    	List<String> datos = listaCuentaAListaString(this.listaCuentas);
    	try {
			archivoUtil.escribirEnArchivo(datos);
		} catch (ArchivoExcepcion e) {
			// TODO: Loguear utilizando nivel ERROR
			// TODO: Lanzar excepcion AplicacionException con un mensaje descriptivo
		}
    }
    
    /**
     * Convierte un listado de cuentas en un listado de string. 
     * @param cuentas listado de cuentas
     * @return lista de strings
     */
    private static List<String> listaCuentaAListaString(List<Cuenta> cuentas){
    	List<String> datos = new ArrayList<String>();
    	for (Cuenta cuenta : cuentas) {
			datos.add(cuenta.toString());
		}
    	return datos;
    }
    
    /**
     * Convierte un listado de strings a un listado de cuentas
     * @param datos lista de string
     * @return lista de cuentas
     */
    private static List<Cuenta> listaStringAListaCuenta(List<String> datos)
    {
    	List<Cuenta> cuentas = new ArrayList<Cuenta>();
    	for (String registro : datos) {
    		
    		String[] columnas = registro.split(",");
			Cuenta cuenta = new Cuenta(Integer.parseInt(columnas[0]), 
					Integer.parseInt(columnas[1]), Double.parseDouble(columnas[2]));
			cuentas.add(cuenta);
		}
    	return cuentas;
    }
}
