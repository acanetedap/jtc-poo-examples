package ejercicios.atm;

public class Deposito extends Transaccion {
	@Override
    public void ejecutar() 
	{
		pantalla.limpiarPantalla();
		pantalla.mostrarMensaje("Ingrese el monto a depositar: ");
		
		Double monto = teclado.obtenerEntradaDouble();
		
		if(ranura.seRecibioSobreDeposito())
		{
			dispensador.alimentarEfectivo(monto);
			baseDatos.acreditar(getNumeroCuenta(), monto);
			pantalla.mostrarMensaje("Ha depositado correctamente: " + monto);
			teclado.hacerPausa();
		}
		
		
    }
}
