package ejercicios.atm.exception;
/**
 * Excepcion personalizada para manejo de errores correspondiente al manejo de archivos
 * @author 
 *
 */ 
public class ArchivoExcepcion extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ArchivoExcepcion(String mensajeError) {
		super(mensajeError);
	}
}
