package ejercicios.atm;

public class Retiro extends Transaccion {
    @Override
    public void ejecutar() {        
        pantalla.limpiarPantalla();
        
        pantalla.mostrarMensaje("\n\nIngrese monto a extraer (como múltiplos de 50.000Gs): ");
        double montoADebitar = teclado.obtenerEntradaDouble();        
        double saldoActual = baseDatos.obtenerSaldoDisponible(numeroCuenta);
        
        if (montoADebitar <= saldoActual) {
              if(montoADebitar % 50000 == 0 && 
            		  dispensador.haySuficienteEfectivoDisponible(montoADebitar))
              {
            	  dispensador.dispensarEfectivo(montoADebitar);
            	  baseDatos.debitar(getNumeroCuenta(), montoADebitar);
            	  Double nuevoSaldo = baseDatos.obtenerSaldoDisponible(getNumeroCuenta());
            	  pantalla.mostrarMensaje("Extraccion exitosa.");
            	  pantalla.mostrarMensaje("Su saldo disponible es: "+ nuevoSaldo);
            	  teclado.hacerPausa();
              } 
              else 
              {
            	pantalla.mostrarMensaje("Error en monto ingresado. Debe ser multiplo de 50.000");
            	teclado.hacerPausa();
            	ejecutar();
              }
        } else {
            pantalla.mostrarMensaje("\n\n\tSaldo Insuficiente!!!");
            teclado.hacerPausa();
        }        
    }
        
}
